## Installation

```
Metacello new
	repository: 'gitlab://AndreasS/gtrepo:master/src';
	baseline: 'Gtrepo';
	load
```

## Load Lepiter
				
After installing with Metacello, you will be able to execute

```
#BaselineOfGtrepo asClass loadLepiter
```

