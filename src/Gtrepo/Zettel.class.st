Class {
	#name : #Zettel,
	#superclass : #Object,
	#instVars : [
		'rawData',
		'title'
	],
	#category : #Gtrepo
}

{ #category : #accessing }
Zettel class >> for: aFileReference [
^self new rawData: aFileReference 
]

{ #category : #accessing }
Zettel >> gtContentFor: aView [
	<gtView>
	^ aView forward
		title: 'Contents';
		priority: 10;
		object: [rawData contents];
		view: #gtStringFor:
]

{ #category : #accessing }
Zettel >> printOn: aStream [
aStream nextPutAll: self title
]

{ #category : #accessing }
Zettel >> rawData: aFileReference [ 
rawData:= aFileReference 
]

{ #category : #accessing }
Zettel >> title [
	^ title ifNil: [title :=rawData contents lines first ]
]
