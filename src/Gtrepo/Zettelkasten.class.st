Class {
	#name : #Zettelkasten,
	#superclass : #Object,
	#instVars : [
		'rawData',
		'zettels'
	],
	#category : #Gtrepo
}

{ #category : #instan }
Zettelkasten class >> for: folderReference [
^self new rawData: folderReference  
]

{ #category : #accessing }
Zettelkasten >> gtZettelsFor: aView [
	<gtView>
	^ aView forward
		title: 'Zettels';
		priority: 10;
		object: [self zettels];
		view: #gtItemsFor:
]

{ #category : #instan }
Zettelkasten >> rawData: folderReference [ 
rawData:=  folderReference
]

{ #category : #accessing }
Zettelkasten >> zettelFiles [
	^ rawData allChildrenMatching: '*.md'
]

{ #category : #accessing }
Zettelkasten >> zettels [
	^ zettels
		ifNil: [ zettels := self zettelFiles collect: [ :f | Zettel for: f ] ]
]
